module gitlab.cern.ch/bnl-omega-go/si5345tool

go 1.17

require gitlab.cern.ch/bnl-omega-go/slowcontrol v0.0.0-20220222213738-6bf7ca5c19ed

require (
	github.com/codehardt/mmap-go v1.0.1 // indirect
	gitlab.cern.ch/bnl-omega-go/axi v0.0.0-20220219041304-c5147b6a5b1a // indirect
	golang.org/x/sys v0.0.0-20220222200937-f2425489ef4c // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	periph.io/x/conn/v3 v3.6.10 // indirect
	periph.io/x/host/v3 v3.7.2 // indirect
)
