//go:build linux || darwin
// +build linux darwin

package main

import (
	"flag"
	"log"

	SlowControl "gitlab.cern.ch/bnl-omega-go/slowcontrol"
)

func main() {

	i2cbus := flag.String("b", "/dev/i2c-3", "bus to be used for I2C communication ex: /dev/i2c-0,AXI etc.")
	devaddr := flag.Uint64("a", 0x68, "I2C address of the device")
	delay := flag.Int("d", 0, "delay in ms between transaction")
	//conffile := flag.String("f", "P_IN1_25M_125MHz", "Frequency to configure in clock chip. Choose between P_IN1_25M_125MHz, FREERUN_125MHz, A_IN1_25M_125MHz, A_IN1_40M_MixMHz")
	conffile := flag.String("f", "/home/root/si5345.csv", "CSV file containing configuration for the clock chip")
	dryrun := flag.Bool("dry", false, "Dry run reading CSV file")
	verbose := flag.Bool("v", false, "print extra verbosity during execution")
	flag.Parse()

	if *dryrun {
		log.Println("WARNING!!! This is a dry-run !!")
	}

	ctrl := SlowControl.NewSi5345Controller(uint16(*devaddr), *i2cbus, *dryrun, *verbose)
	ctrl.ConfigureClock(*conffile, *delay, *dryrun)

}
